<?php
class Migration_Create_room_type extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'description' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'price' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'default' => 'Active'
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('room_type');
	}

	public function down()
	{
		$this->dbforge->drop_table('room_type');
	}
}
