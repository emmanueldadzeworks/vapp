<?php
class Migration_Create_sales extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'client_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'room_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'json_orders' => array(
				'type' => 'TEXT',
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('sales');
	}

	public function down()
	{
		$this->dbforge->drop_table('sales');
	}
}
