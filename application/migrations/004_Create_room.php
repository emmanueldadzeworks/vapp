<?php
class Migration_Create_room extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'room_type_id' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'room_number' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'room_description' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'blueprint' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'picture' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'default' => 'Active'
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('room');
	}

	public function down()
	{
		$this->dbforge->drop_table('room');
	}
}
