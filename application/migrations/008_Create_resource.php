<?php
class Migration_Create_resource extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field( array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'resource' => array(
				'type' => 'bytea',
			),
			'extension' => array(
				'type' => 'character varying',
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('resource');
	}

	public function down()
	{
		$this->dbforge->drop_table('resource');
	}
}
