<?php
class Migration_Create_clients extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'other_names' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'date_of_birth' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '128',
				'null' => TRUE,
			),
			'address' => array(
				'type' => 'TEXT',
			),
			'gender' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'telephone' => array(
				'type' => 'VARCHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'default' => 'Active'
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
			'bio' => array(
				'type' => 'TEXT',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('clients');
	}

	public function down()
	{
		$this->dbforge->drop_table('clients');
	}
}