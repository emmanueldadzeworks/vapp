<?php
class Migration_Set_Super_root_user extends CI_Migration {

	public function up()
	{
		$data = array(
		        'email' 					=> 'emmanuel@zentechgh.com',
		        'first_name' 				=> 'Emmanuel',
		        'last_name' 				=> 'Dadzie',
		        'picture' 					=> 'resource/img/avatar1.png',
		        'admin_type' 				=> 0,
		        'status' 					=> 'Active',
				'password' 					=> 	md5('1234'),
				
		);

		$this->db->insert( 'users', $data);
	}

	public function down()
	{
		$this->db->empty_table( 'users' );
	}
}