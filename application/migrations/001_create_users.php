<?php
class Migration_Create_users extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'admin_type' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '128',
				'null' => TRUE,
			),
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'gender' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'picture' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'motto' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE,
			),
			'bio' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
				'default' => 'Non-Active'
			),
			'last_login' => array(
				'type' => 'timestamp without time zone',
				'null' => TRUE,
			),
			'last_logout' => array(
				'type' => 'timestamp without time zone',
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'timestamp without time zone',
				'null' => TRUE,
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');
	}

	public function down()
	{
		$this->dbforge->drop_table('users' , $fields );
	}
}