<?php
class Migration_Create_food_and_drinks extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field (  array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'price' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'food_and_drinks_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'picture' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'timestamp without time zone',
			),
			'modified' => array(
				'type' => 'timestamp without time zone',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('food_and_drinks');
	}

	public function down()
	{
		$this->dbforge->drop_table('food_and_drinks');
	}
}
