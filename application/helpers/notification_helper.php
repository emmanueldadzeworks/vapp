<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function notification ( $notify )
{
  return "
      <script type='text/javascript'>
      $(document).ready(function() {
        'use strict';
        
        $.toast({
          heading: '".$notify['title']."',
          text: '".$notify["message"]."',
          position: 'top-right',
          loaderBg:'#fec107',
          icon: '".$notify["type"]."',
          hideAfter: 3500, 
          stack: 6
        });
        
      });
                  
      </script>
  ";
}

?>
