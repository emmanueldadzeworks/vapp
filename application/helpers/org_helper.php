<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function curl_request($url , $data ) {
        $CI = & get_instance();
        $session = $CI->session;
        $data["employee"] = $session->userdata('employee');
        
	$string = http_build_query($data);
	// Create a curl handle to domain 2
	$ch = curl_init($url); 
	header('Content-type: application/pdf');

	//put data to send
	curl_setopt($ch, CURLOPT_URL, $url); 
	//configure a POST request with some options
	curl_setopt($ch, CURLOPT_POST, true);

	//put data to send
	curl_setopt($ch, CURLOPT_POSTFIELDS, $string);  


	//this option avoid retrieving HTTP response headers in answer
	//curl_setopt($ch, CURLOPT_HEADER, 0);
	
	//execute request
	curl_exec($ch);
}


function curl_request_returns($url , $data) {


	$string = http_build_query($data);
	// Create a curl handle to domain 2
	$ch = curl_init($url); 

	//put data to send
	curl_setopt($ch, CURLOPT_URL, $url); 
	//configure a POST request with some options
	curl_setopt($ch, CURLOPT_POST, true);

	//put data to send
	curl_setopt($ch, CURLOPT_POSTFIELDS, $string);  

	curl_setopt($ch, CURLOPT_HEADER, 0);
	//Get data to send
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 
	//execute request
	return curl_exec($ch);
}

function curl_request_init($url , $data , $div = null) {


	$string = http_build_query($data);
	// Create a curl handle to domain 2
	$ch = curl_init($url); 

	//put data to send
	curl_setopt($ch, CURLOPT_URL, $url); 
	//configure a POST request with some options
	curl_setopt($ch, CURLOPT_POST, true);

	//put data to send
	curl_setopt($ch, CURLOPT_POSTFIELDS, $string);  

	curl_setopt($ch, CURLOPT_HEADER, 0);
	//Get data to send
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 


	/*execute request
	$html_dom = new simple_html_dom();
	$html_dom->load(curl_exec($ch));
	return $html_dom->find('[name="suggest"]',0)->value;
	*/
	return curl_exec($ch);
}
?>




