<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function btn_edit_model ($uri = null)
{
	return '<a style="padding-left:10px;padding-right:10px;" title="Edit" class="fa fa-pencil-square-o" data-toggle="modal" data-target="#'.$uri.'"></a> ';
}
