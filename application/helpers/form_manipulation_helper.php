<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function d_submit ($pending)
{
    if ($pending > 0) { ?>
        <center>
            <h4 class="header-title m-t-0 m-b-30">You have a Pending Request</h4>
        </center>
    <?php } else { ?>
        <div class="row">
            <div class="form-group text-right m-b-0">
                <button class="btn btn-primary waves-effect waves-light" type="submit">
                    Submit
                </button>
                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                    Cancel
                </button>
            </div>
        </div>
    <?php }
}

function form_input_selector($input = null , $properties = array() , $db_values = null ) { 
        switch ($input) {
            case 'text':
                ?>
                    <div class="form-group m-b-40">
                        <input type="text" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" value="<?=$db_values?>" data-toggle="tooltip" data-placement="bottom" title="<?=humanize($properties->lable)?>" required>
                        <span class="highlight"> </span> 
                        <span class="bar"> </span>
                        <label for="<?=underscore($properties->lable)?>"><?=humanize($properties->lable)?></label>
                    </div>
                <?php
            break;
            case 'number':
                ?>
                    <div class="form-group m-b-40">
                        <input type="number" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" value="<?=$db_values?>" data-toggle="tooltip" data-placement="bottom" title="<?=humanize($properties->lable)?>" required>
                        <span class="highlight"> </span> 
                        <span class="bar"> </span>
                        <label for="<?=underscore($properties->lable)?>"><?=humanize($properties->lable)?></label>
                    </div>
                <?php
            break;
            
            case 'textarea':
                ?>
                    <div class="form-group m-b-40">
                        <textarea class="form-control" rows="4" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" data-toggle="tooltip" data-placement="bottom" title="<?=humanize($properties->lable)?>" required> <?=$db_values?> </textarea>
                        <span class="highlight"> </span> 
                        <span class="bar"> </span>
                        <label for="<?=underscore($properties->lable)?>"><?=humanize($properties->lable)?></label>
                    </div>
                <?php
            break;
            case 'password':
                ?>
                    <div class="form-group m-b-40">
                        <input type="password" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" required>
                        <span class="highlight"> </span> 
                        <span class="bar"> </span>
                        <label for="<?=underscore($properties->lable)?>"><?=humanize($properties->lable)?></label>
                    </div>
                <?php
            break;
            case 'select':
                ?>
                    <select class="form-control select2" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" required>
                        <option value="">Select</option>
                        <?php foreach(explode("|", $properties->select_array) as $key => $value){ ?>
                            <?php if(trim($value) === trim($db_values) ){ ?>
                                <option selected><?=trim($value)?></option>
                            <?php } else { ?>
                                <option><?=trim($value)?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                <?php
            break;
            case 'select_query':
                $CI = get_instance();
                $CI->load->model('navigation_m');
                $CI->load->model('view_forms_m');
                $CI->load->model('form_data_m');
                $components = explode(")(" , $properties->select_query);
                $components[0] = str_replace("(", "", $components[0]);
                $comp = trim(str_replace(")", "", $components[1]));

                $id_to_nav = $CI->navigation_m->get_by(array("slug"=> $components[0] ), true);
                $id_to_col = $CI->view_forms_m->get_by(array("navigation_id"=> $id_to_nav->id ),  true);
                $results = $CI->form_data_m->get_by(array("view_form_id"=> $id_to_col->id ));

                ?>
                    <select class="form-control select2"  id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>" required>
                        <option value="">Select</option>
                        <?php foreach($results as $key => $value){ ?>
                            <?php if(trim(json_decode($value->array_data)->$comp) === trim($db_values) ){ ?>
                                <option value="<?=$value->id?>" selected><?=trim(json_decode($value->array_data)->$comp)?></option>
                            <?php } else { ?>
                                <option value="<?=$value->id?>"><?=trim(json_decode($value->array_data)->$comp)?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                <?php
            break;
            
            default:
                ?>
                <div class="form-group m-b-40">
                    <input type="text" class="form-control" id="<?=underscore($properties->lable)?>" name="<?=underscore($properties->lable)?>"  value="<?=$db_values?>" data-toggle="tooltip" data-placement="bottom" title="<?=humanize($properties->lable)?>" required>
                    <span class="highlight"> </span> 
                    <span class="bar"> </span>
                    <label for="<?=underscore($properties->lable)?>"><?=humanize($properties->lable)?></label>
                </div>
                <?php
            break;
        } ?>
        

    
    <?php
    
    
}


function forms_simplifier($form_view = null , $db_values = null ) { ?>
    <?=form_open_multipart(null , array("class" => "floating-labels")); ?>
        <?php
            foreach(json_decode($form_view->array_form) as $extract){
                foreach($extract as $key => $value){
                    $haskey = underscore($value->lable);
                    $show_db_values = $db_values === null ? "" : $db_values->$haskey;
                    form_input_selector($value->input_type , $value , $show_db_values );
                }
                break;
            }
            d_submit(0);
        ?>
    <?=form_close(); ?>


<?php } 


function data_table_headers($form_view = null ) { 
    foreach(json_decode($form_view->array_form) as $extract){
        foreach($extract as $key => $value){
            ?><th><?=$value->lable?></th><?php
        }
        break;
    }
} 

function data_table_headers_array($form_view = null ) { 
    $return=array();
    foreach(json_decode($form_view->array_form) as $extract){
        foreach($extract as $key => $value){
            $return[] = underscore($value->lable); 
        }
        break;
    }
    return (object) $return;
} 