<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function var_divition ($user = null)
{
	$CI =& get_instance();
	$array = array(
		'Personal_Details' => array(
			'Personal_Details' => array(
				'title_id' => array(
					'input' => 'select',
					'select' => form_select_array_generator($CI->universal_m->get(NULL, FALSE , "common.titles"), "title_id" , "description"),//'text',
					'dis' => $CI->universal_m->get($user->title_id, true , "common.titles" ,false ,"title_id"),
					'dis_col' => 'description',
					'name' => 'title_id',
					'label' => 'Title',
					'validation' => 'trim'
				), 
				'firstname' => array(
					'input' => 'text',
					'name' => 'firstname',
					'label' => 'First Name',
					'validation' => 'trim'
				), 
				'lastname' => array(
					'input' => 'text',
					'name' => 'lastname',
					'label' => 'Last Name',
					'validation' => 'trim',
				), 
				'othernames' => array(
					'input' => 'int',
					'name' => 'othernames',
					'label' => 'Other Names',
					'validation' => 'trim', 
				), 
				'gender' => array(
					'input' => 'selectlist',
                                        'list' => 'MALE|FEMALE',
					'name' => 'gender',
					'label' => 'Gender',
					'validation' => 'trim', 
				), 
				'date_of_birth' => array(
					'input' => 'date',
					'name' => 'date_of_birth',
					'label' => 'Date of Birth',
					'validation' => 'trim', 
				), 
				'home_town' => array(
					'input' => 'text',
					'name' => 'home_town',
					'label' => 'Home town',
					'validation' => 'trim', 
				), 
				'marital_status' => array(
					'input' => 'selectlist',
                                        'list' => 'SINGLE|MARRIED|DIVORCED|WIDOWED',
					'name' => 'marital_status',
					'label' => 'Marital Status',
					'validation' => 'trim', 
				), 
			),
			'Contact_Details' => array(
				'mailing_address' => array(
					'input' => 'textarea',
					'name' => 'mailing_address',
					'label' => 'Mailing Address',
					'validation' => 'trim', 
				), 
				'residential_address' => array(
					'input' => 'text',
					'name' => 'residential_address',
					'label' => 'Residential Address',
					'validation' => 'trim', 
				), 
				'email' => array(
					'input' => 'email',
					'name' => 'email',
					'label' => 'Email',
					'validation' => 'trim', 
				), 
				'telephone' => array(
					'input' => 'number',
					'pattern' => '[0-9]{5}[-][0-9]{7}[-][0-9]{1}',
					'name' => 'telephone',
					'label' => 'Telephone',
					'validation' => 'trim', 
				), 
			),
			'Relations' => array(
				'fathers_name' => array(
					'input' => 'text',
					'name' => 'fathers_name',
					'label' => 'Father\'s Name',
					'validation' => 'trim', 
				), 
				'mothers_name' => array(
					'input' => 'text',
					'name' => 'mothers_name',
					'label' => 'Mother\'s Name',
					'validation' => 'trim', 
				), 
				'spouse_name' => array(
					'input' => 'text',
					'name' => 'spouse_name',
					'label' => 'Name of Spouse',
					'validation' => 'trim', 
				), 
			), 
			'Next_of_Kin_Details' => array(
				'next_of_kin' => array(
					'input' => 'text',
					'name' => 'next_of_kin',
					'label' => 'Next of Kin',
					'validation' => 'trim', 
				), 
				'next_of_kin_address' => array(
					'input' => 'text',
					'name' => 'next_of_kin_address',
					'label' => 'Address',
					'validation' => 'trim', 
				), 
				'next_of_kin_telephone' => array(
					'input' => 'text',
					'name' => 'next_of_kin_telephone',
					'label' => 'Telephone',
					'validation' => 'trim', 
				), 
			), 
		), 
		'Employment_Details' => array(
			'Employment_Details' => array(
				'ddub_id' => array(
					'input' => 'select',
					'select' => form_select_array_generator($CI->universal_m->get(NULL, FALSE , "hr.ddubs"), "ddub_id" , "description"),//'text',
					'dis' => $CI->universal_m->get($user->ddub_id, true , "hr.ddubs" ,false ,"ddub_id"),
					'dis_col' => 'description',
					'name' => 'ddub_id',
					'label' => 'DDUB',
					'validation' => 'trim', 
				), 
				'position_id' => array(
					'input' => 'select',
					'select' => form_select_array_generator($CI->universal_m->get(NULL, FALSE , "hr.positions"), "position_id" , "description"),//'text',
					'dis' => $CI->universal_m->get($user->position_id, true , "hr.positions" ,false ,"position_id"),
					'dis_col' => 'description',
					'name' => 'position_id',
					'label' => 'Position',
					'validation' => 'trim', 
				),
				'category_id' => array(
					'input' => 'select',
					'select' => form_select_array_generator($CI->universal_m->get(NULL, FALSE , "hr.categories"), "category_id" , "description"),//'text',
					'dis' => $CI->universal_m->get($user->category_id, true , "hr.categories" ,false ,"category_id"),
					'dis_col' => 'description',
					'name' => 'category_id',
					'label' => 'Category',
					'validation' => 'trim', 
				), 
				/*
				'job_location_id' => array(
					'input' => 'text',
					'name' => 'job_location_id',
					'label' => 'Job Location',
					'validation' => 'trim', 
				),
				'job_title_id' => array(
					'input' => 'text',
					'name' => 'job_title_id',
					'label' => 'Job Title',
					'validation' => 'trim', 
				), 
				*/
				'employment_date' => array(
					'input' => 'text',
					'name' => 'employment_date',
					'label' => 'Date of Employment',
					'validation' => 'trim', 
				), 
                                'employment_type' => array(
					'input' => 'selectlist',
                                        'list' => 'PERMANENT|FIXED TERM',
					'name' => 'employment_type',
					'label' => 'Employment Type',
					'validation' => 'trim', 
				), 
                                'employment_status' => array(
					'input' => 'selectlist',
                                        'list' => 'CONFIRMED|PROBATION',
					'name' => 'employment_status',
					'label' => 'Employment Status',
					'validation' => 'trim', 
				),
				'confirmation_date' => array(
					'input' => 'text',
					'name' => 'confirmation_date',
					'label' => 'Date of Confirmation',
					'validation' => 'trim', 
				), 
			), 
			'Payment_Details' => array(
				/* Both -----pay_grade_notch*/
				'grade_id' => array(
					'input' => 'select',
					'disabled' => true,
					'select' => form_select_array_generator($CI->universal_m->get(NULL, FALSE , "payroll.grades"), "grade_id" , "description"),//'text',
					'dis' => $CI->universal_m->get($user->grade_id, true , "payroll.grades" ,false ,"grade_id"),
					'dis_col' => 'description',
					'ajax' => '<script>
								$(document).ready(function(){
									$("#f_grade_id").change(function(){
										$.ajax({url: "'.base_url().'control/ajax/load_paylevel/"+$(\'#f_grade_id :selected\').val() , success: function(result){
											$("#f_grade_level_id").html(result);
										}});
									});
								});
								</script>',
					'name' => 'grade_id',
					'label' => 'Pay Grade',
					'validation' => 'trim', 
				), 
				'grade_level_id' => array(
					'input' => 'select',
					'disabled' => true,
					'select' => form_select_array_generator($CI->universal_m->get(NULL, FALSE , "payroll.grade_levels"), "grade_level_id" , "description"),//'text',
					'dis' => $CI->universal_m->get($user->grade_level_id, true , "payroll.grade_levels" ,false ,"grade_level_id"),
					'dis_col' => 'description',
					'name' => 'grade_level_id',
					'label' => 'Grade Level',
					'validation' => 'trim', 
				), 
				'basic_salary' => array(
					'input' => 'text',
					'disabled' => true,
					'nb_format' => true,
					'name' => 'basic_salary',
					'label' => 'Basic Salary',
					'validation' => 'trim', 
				), 
			), 
			'Bank_Details' => array(
				/*
				'bank_branch_id_main' => array(
					'input' => 'text',
					'name' => 'bank_branch_id_main',
					'label' => 'Bank Account',
					'validation' => 'trim', 
				), 
				'bank_branch_id' => array(
					'input' => 'text',
					'name' => 'bank_branch_id',
					'label' => 'Bank Branch',
					'validation' => 'trim', 
				), 
				*/
				'bank_account_number' => array(
					'input' => 'text',
					'name' => 'bank_account_number',
					'label' => 'Account Number',
					'validation' => 'trim', 
				), 
			), 
			'Tax_Relief_Details' => array(
				'bank_branch_id_main' => array(
					'input' => 'checkbox',
					'name' => 'marriage_relief',
					'label' => 'Marriage Relief',
					'validation' => 'trim', 
				), 
				'number_of_dependent_children' => array(
					'input' => 'number',
					'name' => 'number_of_dependent_children',
					'label' => 'Number of Dependent Children',
					'validation' => 'trim', 
				), 
				'number_of_dependent_relatives' => array(
					'input' => 'number',
					'name' => 'number_of_dependent_relatives',
					'label' => 'Number of Dependent Relatives',
					'validation' => 'trim', 
				), 
				'physically_challenged' => array(
					'input' => 'text',
					'name' => 'physically_challenged',
					'label' => 'Physically Challenged',
					'validation' => 'trim', 
				), 
				'under_training' => array(
					'input' => 'text',
					'name' => 'under_training',
					'label' => 'Under Training',
					'validation' => 'trim', 
				), 
			), 
			'Identifications' => array(
				'social_security_name' => array(
					'input' => 'text',
					'name' => 'social_security_name',
					'label' => 'Social Security Name',
					'validation' => 'trim', 
				), 
				'social_security_number' => array(
					'input' => 'text',
					'name' => 'social_security_number',
					'label' => 'Social Security Number',
					'validation' => 'trim', 
				), 
				'tax_identification_number' => array(
					'input' => 'text',
					'name' => 'tax_identification_number',
					'label' => 'Tax Identification Number',
					'validation' => 'trim', 
				), 
			), 
		), 
	);

	return $array;
}


function form_select_array_generator ($data , $col_key , $col_val)
{
    $returns = new stdClass();
	foreach ($data as $key => $value) {
		$ALAN_KEY = $value->$col_key;
		$ALAN_VALUE = $value->$col_val;
		$returns->$ALAN_KEY = $ALAN_VALUE;
	}
    return $returns;
	
}