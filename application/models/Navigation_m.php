<?php
class Navigation_m extends MY_Model
{
	
	protected $_table_name = 'navigation';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

	public $rules = array(
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|is_unique[navigation.title]'
		), 
		'order' => array(
			'field' => 'order', 
			'label' => 'Order', 
			'rules' => 'trim|required'
		)
	);

	function __construct ()
	{
		parent::__construct();
	}

	function get_active ()
	{
		return $this->get_by(array("status" => "Active"));
	}

	

}