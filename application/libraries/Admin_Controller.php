<?php
class Admin_Controller extends MY_Controller
{

	function __construct ()
	{
		parent::__construct();
		$this->load->model('user_m');
		

		/*  //Include PhpMailer
		require FCPATH.'vendor/autoload.php';
		require FCPATH.'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
		require FCPATH.'vendor/phpmailer/phpmailer/class.phpmailer.php';
		*/

		// Login check
		$user_agent = explode(")",$_SERVER['HTTP_USER_AGENT']);
		$user_agent_one = explode("(",$user_agent[0]);
		$user_agent_two = explode(";",$user_agent_one[1]);


		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
			$this->data['user_agent']['browser'] = 'Internet explorer';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'MSIE/', 1);
			$v_count = strlen("MSIE/");
		} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
			$this->data['user_agent']['browser'] = 'Mozilla Firefox';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Firefox/', 1);
			$v_count = strlen("Firefox/");
			$user_agent = explode(")",$_SERVER['HTTP_USER_AGENT']);
			$user_agent_one = explode("(",$user_agent[0]);
			$user_agent_two = explode(";",$user_agent_one[1]);
	
	
			if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
				$this->data['user_agent']['browser'] = 'Internet explorer';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'MSIE/', 1);
				$v_count = strlen("MSIE/");
			} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
				$this->data['user_agent']['browser'] = 'Mozilla Firefox';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Firefox/', 1);
				$v_count = strlen("Firefox/");
			} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
				$this->data['user_agent']['browser'] = 'Google Chrome';
				$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Chrome/', 1);
				$v_count = strlen("Chrome/");
			} else {
				$this->data['user_agent']['browser'] = 'Something else';
			}
			$this->data['user_agent']['browser_version'] 	= substr($_SERVER['HTTP_USER_AGENT'] , (int) $pos+$v_count , 4);
			$this->data['user_agent']['OS'] 				= trim($user_agent_two[1]);
			$this->data['user_agent']['browser_agent'] 		= trim(explode("(" , trim($user_agent[0]))[0]);
	
		} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
			$this->data['user_agent']['browser'] = 'Google Chrome';
			$pos = strpos($_SERVER['HTTP_USER_AGENT'] , 'Chrome/', 1);
			$v_count = strlen("Chrome/");
		} else {
			$this->data['user_agent']['browser'] = 'Something else';
		}
		$this->data['user_agent']['browser_version'] 	= substr($_SERVER['HTTP_USER_AGENT'] , (int) $pos+$v_count , 4);
		$this->data['user_agent']['OS'] 				= trim($user_agent_two[1]);
		$this->data['user_agent']['browser_agent'] 		= trim(explode("(" , trim($user_agent[0]))[0]);


		$uris_loout = array(
			'welcome/logout', 
		);
		$uris_login = array(
			'welcome/index', 
			'welcome/reset', 
			'welcome', 
			'', 
		);
		$uris_lock = array(
			'welcome/lock/account', 
			'welcome/lock', 
		);


		//in login
		if (in_array(uri_string(), $uris_login) == TRUE) 
		{
			if ($this->user_m->loggedin() == TRUE) {
				redirect("generic/dashboard");
			}
		}


		//in dashboard
		if (in_array(uri_string(), $uris_login) == FALSE) 
		{
			if ($this->user_m->loggedin() == FALSE) {
				redirect();
			} else {
				//Autoload data
			}
		} 
		
		//in lock 
		if (in_array(uri_string(), $uris_lock) == FALSE) 
		{
			if ($this->user_m->lockin() == TRUE) {
				redirect('welcome/lock/account');
			}			
		}


        if(!empty($this->session->flashdata('success'))){
            $this->data['notify'][] = array('title' => 'Success', 'message' => $this->session->flashdata('success') , 'type' => 'success' );
        }
        

        if(!empty($this->session->flashdata('error'))){
                $this->data['notify'][] = array('title' => 'Oops', 'message' => $this->session->flashdata('error'), 'type' => 'error' );
		}
		
		//setcookie("test_cookie", "test", time() + 3600, '/');
		if(count($_COOKIE) < 0) {
		    echo "<script>alert('Cookies are disabled. \n Pls enable for full funtionality')</script>";
		}

	}
}