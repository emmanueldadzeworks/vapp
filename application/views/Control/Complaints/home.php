        <div class="wrapper">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                        </div>
                        <h4 class="page-title"> Complaints </h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-8">
                        <div class="card-box">

                            <h4 class="header-title m-t-0 m-b-30">Submitting Complaints</h4>

                            <?php $class = array('id' => 'fileupload' ); ?>
                            <?php echo form_open_multipart(null , $class); ?>
                                <?=validation_errors()?>

                                <div class="form-group">
                                    <label for="subject">Subject *</label>
                                    <input type="text" class="form-control" name="subject" required>
                                </div>

                                <div class="form-group">
                                    <label for="messages"> Message </label>
                                    <textarea class="form-control" rows="5" name="message" id="messages"></textarea>
                                </div>

                                <div class="repeater-custom-show-hide">
                                    <button data-repeater-create type="button" class="btn btn-purple btn-trans waves-effect w-md waves-purple m-b-5">Add</button>
                                    <div data-repeater-list="files">
                                        <div data-repeater-item>
                                            <div class="dropify-wrapper has-preview" style="height: 314px;">
                                               <div class="dropify-message">
                                                  <span class="file-icon"></span> 
                                                  <p>Drag and drop a file here or click</p>
                                                  <p class="dropify-error">Ooops, something wrong .</p>
                                               </div>
                                               <div class="dropify-loader"></div>
                                               <div class="dropify-errors-container">
                                                  <ul></ul>
                                               </div>
                                               <input type="file" name="image" class="dropify" data-height="300" data-max-file-size="5M" accept="image/*"><button type="button" class="dropify-clear">Remove</button>
                                               <div class="dropify-preview">
                                                  <span class="dropify-render"></span>
                                                  <div class="dropify-infos">
                                                     <div class="dropify-infos-inner">
                                                        <p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>
                                                        <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit"> Submit </button>
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5"> Cancel </button>
                                </div>

                            <?php echo form_close(); ?>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-4">
                        <div class="card-box">

                            <h4 class="header-title m-t-0 m-b-30">My Complaints</h4>

                            <ul class="list-group m-b-0 user-list">
                                <?php $azRange = range('A', 'Z'); ?>
                                <?php foreach ($public_user_complaint_recent as $key => $value) { ?>
                                    <li class="list-group-item">
                                        <div class="avatar right">
                                            <?php $count_img=1;?>
                                            <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                <?php if (strpos($json_key, '_image')) { ?>
                                                    <?php if ($count_img > 2) { ?>
                                                        <div class="more_background right" style="background: url(<?=base_url($json_value)?>);" >
                                                          <div class="more_transbox">
                                                            <p>
                                                                <a href="#view_modale<?=mb_strtolower($azRange[$key])?>" class="color_white" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">view more....</a>
                                                            </p>
                                                          </div>
                                                        </div>
                                                        <?php break; ?>
                                                    <?php } ?>
                                                    <a href="<?=base_url($json_value)?>" class="image-popup" title="Proof">
                                                        <img src="<?=base_url($json_value)?>" class="img-thumbnail  sm_image" alt="profile-image">
                                                    </a>
                                                    <?php $count_img++; ?>
                                                <?php } ?>
                                                
                                            <?php } ?>
                                        </div>
                                        <a href="#view_modale<?=mb_strtolower($azRange[$key])?>" class="user-list-item waves-effect waves-light m-r-5 m-b-10" style="max-width: 70%;"  data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">
                                            <div class="avatar">
                                                <i class="ti-arrow-right"></i>
                                            </div>
                                            <div class="user-desc">
                                                <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                    <?php if (!strpos($json_key, '_image')) { ?>
                                                        <span class="name"><?=$json_key?> :: <?=$json_value?> </span>
                                                    <?php } ?>
                                                <?php } ?>
                                                <span class="desc"><?php echo humanize($value->request_type); ?></span>
                                                
                                                <?php
                                                    switch ($value->status) {
                                                        case NULL:
                                                            echo '<span class="label label-purple">Work in Progress</span>';
                                                            break;
                                                        case 'Pending':
                                                            echo '<span class="label label-purple">Work in Progress</span>';
                                                            break;
                                                        case 'Approved':
                                                            echo '<span class="label label-success">Approved</span>';
                                                            break;
                                                        case 'Reject':
                                                            echo '<span class="label label-success">Rejected by user </span>';
                                                            break;
                                                        case 'Declined':
                                                            echo '<span class="label label-success">Rejected by Adminustrator </span>';
                                                            break;
                                                        
                                                        default:
                                                            echo '<span class="label label-warning">Coming soon</span>';
                                                            break;
                                                    }


                                                ?>
                                                
                                            </div>
                                        </a>

                                        <div id="view_modale<?=mb_strtolower($azRange[$key])?>" class="modal-demo">
                                            <button type="button" class="close" onclick="Custombox.close();">
                                                <span>&times;</span><span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="custom-modal-title"><?php echo humanize('Details of Request'); ?></h4>
                                            <div class="custom-modal-text">
                                                <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                    <?php if (!strpos($json_key, '_image')) { ?>
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label"><?=humanize(strtoupper($json_key))?></label>
                                                                <div class="col-md-10"><?=$json_value?> </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="row">
                                                <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                    <?php if (strpos($json_key, '_image')) { ?>
                                                        <a href="<?=base_url($json_value)?>" target="_blank" title="Proof">
                                                            <img src="<?=base_url($json_value)?>" class="img-thumbnail m_image" alt="profile-image">
                                                        </a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <style type="text/css">
                    .datepicker table tr td.active, .datepicker table tr td.active:hover, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled:hover {
                        background-color: rgba(113, 182, 249, 0) !important;
                        background-image: none;
                        box-shadow: none;
                        text-shadow: none;
                    }

                    .datepicker table tr td.today, .datepicker table tr td.today.disabled, .datepicker table tr td.today.disabled:hover, .datepicker table tr td.today:hover {
                        background-color: #435966;
                        background-image: -moz-linear-gradient(to bottom,#fdd49a,#fdf59a);
                        background-image: -ms-linear-gradient(to bottom,#fdd49a,#fdf59a);
                        background-image: -webkit-gradient(linear,0 0,0 100%,from(#435966),to(#8f9ea7));
                        background-image: -webkit-linear-gradient(to bottom,#fdd49a,#fdf59a);
                        background-image: -o-linear-gradient(to bottom,#fdd49a,#fdf59a);
                        background-image: linear-gradient(to bottom,#435966,#8f9ea7);
                        background-repeat: repeat-x;
                        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdd49a', endColorstr='#fdf59a', GradientType=0);
                        border-color: #435966 #8f9ea7 #71b6f9;
                        border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);
                        filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
                        color: #000;
                    }
                </style>
