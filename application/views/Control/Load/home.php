
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?=$page->title?></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="#">Page</a></li>
                            <li class="active"><?=$page->title?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <!-- Tabstyle start -->
                            <section class="m-t-40">
                                <div class="sttabs tabs-style-iconbox">
                                    <nav>
                                        <ul>
                                            <li><a href="#section-iconbox-1" class="sticon ti-upload"><span>Forms</span></a></li>
                                            <li><a href="#section-iconbox-2" class="sticon ti-home"><span>Home</span></a></li>
                                            <li><a href="#section-iconbox-3" class="sticon ti-gift"><span>Data table</span></a></li>
                                        </ul>
                                    </nav>
                                    <div class="content-wrap">
                                        <section id="section-iconbox-1">
                                            <h2> <!-- Forms --> </h2>
                                            <?=$form_view == null ? '<p class="text-muted m-b-30"> No Forms Has been set up</p>' : forms_simplifier($form_view , json_decode($form_data_single->array_data) )?>
                                            
                                        </section>
                                        <section id="section-iconbox-2">
                                            <h3><?=$page->title?></h3>
                                            <p><?=$page->body?></p>
                                        </section>
                                        <section id="section-iconbox-3">
                                            <h2>Data</h2>
                                            <?php if($form_view){ ?>
                                                <p class="text-muted m-b-30">Export data to Copy, CSV, Excel, PDF & Print</p>
                                                <div class="table-responsive">
                                                    <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <?=data_table_headers($form_view )?>
                                                                <td> operations </td>
                                                            </tr>
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <?=data_table_headers($form_view )?>
                                                                <td> operations </td>
                                                            </tr>
                                                        </tfoot>
                                                        <tbody>
                                                            <?php foreach($form_data as $extract ){ ?>
                                                                <tr>
                                                                    <?php foreach(data_table_headers_array($form_view) as $value){?>
                                                                        <td><?=!isset(json_decode($extract->array_data)->$value) ? "empty" : json_decode($extract->array_data)->$value?></td>
                                                                    <?php } ?>

                                                                    <td>
                                                                        <?=btn_edit(base_url('/generic/page/load/'.$page_title.'/'.$extract->id))?>
                                                                    </td>
                                                                </tr>

                                                            <?php } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } else { ?>
                                                <p class="text-muted m-b-30"> No record found</p>
                                            <?php } ?>
                                        </section>
                                            
                                    </div>
                                    <!-- /content -->
                                </div>
                                <!-- /tabs -->
                            </section>

                        </div>
                    </div>
                </div>