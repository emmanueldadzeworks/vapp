<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	$meta = array(
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'description',
				'content' => 'My Great Generic application'
		),
		array(
				'name' => 'keywords',
				'content' => 'view, navigation, form, query'
		),
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'Content-type',
				'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
		)
	);
json_decode($properties->data)
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title><?=json_decode($properties->data)->company_name?></title>
<?=meta($meta)?>
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>resource/images/logo.png">
<!-- Bootstrap Core CSS -->
<?php echo link_tag('resource/bootstrap/dist/css/bootstrap.min.css'); ?>
<!-- animation CSS -->
<?php echo link_tag('resource/css/animate.css'); ?>
<!-- Custom CSS -->
<?php echo link_tag('resource/css/style.css'); ?>
<!-- color CSS -->
<?php echo link_tag('resource/css/colors/blue.css'); ?>
<style>
	.login-register {
		background: url(<?php echo base_url(); ?>resource/images/login-register.png) center center/cover no-repeat!important;
	}
	.white-box {
		background: #fff0 !important;
	}
	.login-box {
    background: #ffffffc2;
}
</style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
	<?php $loginform = array("id" =>  "loginform" , "class" => "form-horizontal form-material"); ?>
	<?php echo form_open_multipart(null , $loginform); ?>
        <a href="javascript:void(0)" class="text-center db"><img src="<?php echo base_url(); ?>resource/images/logo.png" alt="Home" style="height:100px;"/></a>  
        <h3 class="box-title m-t-40 m-b-0">Register Now</h3><small>Create your account and enjoy</small> 
        <p><?=$this->session->flashdata('error')?></p>
        <p><?=$this->session->flashdata('success')?></p>
        <?=validation_errors()?>
        <div class="form-group m-t-20">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="First Name" name="first_name">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email" name="email">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="Password" name="password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="Confirm Password" name="password_confirm">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" type="checkbox" required>
              <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
            </div>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Already have an account? <a href="<?=base_url()?>" class="text-primary m-l-5"><b>Sign In</b></a></p>
          </div>
        </div>
	  <?php echo form_close(); ?>


	  <?php $recoverform = array("id" =>  "recoverform" , "class" => "form-horizontal"); ?>
	  <?php echo form_open_multipart(null , $recoverform); ?>
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
	  <?php echo form_close(); ?>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>resource/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>resource/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>resource/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>resource/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>resource/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>resource/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url(); ?>resource/dist/jQuery.style.switcher.js"></script>
</body>

</html>
