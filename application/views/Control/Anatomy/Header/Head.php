
<!DOCTYPE html><?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	$meta = array(
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'description',
				'content' => 'My Great Generic application'
		),
		array(
				'name' => 'keywords',
				'content' => 'view, navigation, form, query'
		),
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'Content-type',
				'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
		),
		array(
				'name' => 'author',
				'content' => 'Emmanuel Kwabena Dadzie tel 0209137654'
		)
	);
?>
<html lang="en">

<head>

<?=meta($meta)?>
<!-- Bootstrap Core CSS -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>resource/images/logo.png">
    <title>Guest House Management System</title>
    <!-- Bootstrap Core CSS -->
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
	<?php echo link_tag('resource/bootstrap/dist/css/bootstrap.min.css'); ?>
    <!-- Menu CSS -->
	<?php echo link_tag('resource/dist/sidebar-nav.min.css'); ?>
	
	<!-- toast CSS -->
	<?php echo link_tag('resource/dist/jquery.toast.css'); ?>
	
	<!-- morris CSS -->
	<?php echo link_tag('resource/dist/morris.css'); ?>
	
	<!-- animation CSS -->
	<?php echo link_tag('resource/css/animate.css'); ?>
	
	<!-- Custom CSS -->
	<?php echo link_tag('resource/css/style.css'); ?>
	
	<!-- color CSS -->
	<?php echo link_tag('resource/css/colors/default.css'); ?>
	
	<!-- page CSS -->
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="https://wrappixel.com/demos/admin-templates/pixeladmin/plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    
</head>

<body style="zoom: 0.8;">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
	</div>
