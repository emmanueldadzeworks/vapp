<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('Control/Anatomy/Header/Head'); ?>
<?php $this->load->view('Control/Anatomy/Header/Header'); ?>

<?php $this->load->view('Control/'.$content); ?>

    
<?php $this->load->view('Control/Anatomy/Footer'); ?>

<!--Start of Tawk.to Script 
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/56d9f74c89de54b41e935f68/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>-->
