        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Profile</h4>
                    </div>
                </div>

                <script>
                    var loadFile = function(event) {
                        var output = document.getElementById('output');
                        output.src = URL.createObjectURL(event.target.files[0]);
                        document.getElementById("appear_submit").innerHTML = '<button type="submit" class="btn btn-primary waves-effect waves-light btn-lg m-b-5">Submit</button>';
                    };
                </script>
                <style>
                    label.filebutton {
                        width:120px;
                        height:40px;
                        overflow:hidden;
                        position:relative;
                    }

                    label span input {
                        z-index: 999;
                        line-height: 0;
                        font-size: 50px;
                        position: absolute;
                        top: -2px;
                        left: -700px;
                        opacity: 0;
                        filter: alpha(opacity = 0);
                        -ms-filter: "alpha(opacity=0)";
                        cursor: pointer;
                        _cursor: hand;
                        margin: 0;
                        padding:0;
                    }
                </style>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="bg-picture card-box">
                            <div class="profile-info-name">
                                <?php echo form_open_multipart("control/Profile/picture", array("class" => "img-thumbnail", "style" => "border: 0px solid #EBEFF2;")); ?>
                                    <div class="left">
                                        <?php if ($picture_data_pending == 0) { ?>
                                            <span id="appear_submit"></span>
                                            <label class="filebutton fa fa-pencil">
                                                <span><input accept="image/*" type="file" onchange="loadFile(event)" id="profile_picture" name="profile_picture"></span>
                                            </label>
                                        <?php } else { ?>
                                            Pending Request
                                        <?php }?>
                                        <img id="output" src="<?php echo $user_picture; ?>" alt="profile-image" style="width:200px; height:200px;">
                                    </div>
                                <?php echo form_close(); ?>
                                <div class="profile-info-detail right">
                                    <h3 class="m-t-0 m-b-0" style="color: #000000;"><?php echo _l($user->employee); ?></h3>

                                    <p class="text-muted m-b-20">
                                        <i>Grade : <?php echo _l($user->grade); ?></i>
                                        <br />
                                        <i>Staff ID : <?php echo _l($user->staff_number); ?></i>
                                        <br />
                                    </p>
                                    <p><?php echo _l($public_user->bio); ?></p>

                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!--/ meta -->


                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <div class="dropdown pull-right">
                                        </div>

                                        <h4 class="header-title m-t-0 m-b-30">Details</h4>
                                        <?php echo form_open_multipart(); ?>
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <ul class="nav nav-tabs">
                                                        <?php $count_head=0; ?>
                                                        <?php foreach (var_divition($user) as $key => $value) { ?>
                                                            <li role="presentation" class="<?php echo $count_head === 0 ? 'active' : '' ; ?>">
                                                                <a href="<?php echo '#'.$key; ?>" role="tab" data-toggle="tab" aria-expanded="<?php echo $count_head === 0 ? 'true' : 'false' ; ?>"><?php echo humanize($key); ?></a>
                                                            </li>
                                                            <?php $count_head++; ?>
                                                        <?php } ?>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <?=form_tab_generator($user)?>
                                                    </div>
                                                </div><!-- end col -->

                                            </div>
                                            <!-- end row -->
                                            <?php if ($pending > 0) { ?>
                                                <center>
                                                    <p>
                                                        <h4 class="header-title m-t-0 m-b-30">You have a Pending Request</h4>
                                                    </p>
                                                </center>
                                            <?php } else { ?>
                                            <p>
                                                <button type="submit" class="btn btn-block btn-sm btn-success waves-effect waves-light"> <?php echo humanize('Send request for changes'); ?> </button>
                                            </p>
                                            <?php } ?>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div><!-- end col -->
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-3">
                        <div class="card-box">
							<!--
                            <div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            -->


                            <h4 class="header-title m-t-0 m-b-30">My Requests</h4>

                            <ul class="list-group m-b-0 user-list">
                                <?php $azRange = range('A', 'Z'); ?>
                                <?php foreach ($public_user_bio_recent as $key => $value) { ?>
                                    <li class="list-group-item">
                                        <div class="avatar right">
                                            <?php $count_img=1;?>
                                            <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                <?php if (strpos($json_key, '_image')) { ?>
                                                    <?php if ($count_img > 2) { ?>
                                                        <div class="more_background right" style="background: url(<?=base_url($json_value)?>);" >
                                                          <div class="more_transbox">
                                                            <p>
                                                                <a href="#view_modale<?=mb_strtolower($azRange[$key])?>" class="color_white" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">view more....</a>
                                                            </p>
                                                          </div>
                                                        </div>
                                                        <?php break; ?>
                                                    <?php } ?>
                                                    <a href="<?=base_url($json_value)?>" class="image-popup" title="Proof">
                                                        <img src="<?=base_url($json_value)?>" class="img-thumbnail  sm_image" alt="profile-image">
                                                    </a>
                                                    <?php $count_img++; ?>
                                                <?php } ?>
                                                
                                            <?php } ?>
                                        </div>
                                        <a href="#view_modale<?=mb_strtolower($azRange[$key])?>" class="user-list-item waves-effect waves-light m-r-5 m-b-10" style="max-width: 70%;" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">
                                            <div class="avatar">
                                                <i class="ti-arrow-right"></i>
                                            </div>
                                            <div class="user-desc">
                                                <?php /* foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                    <?php if (!strpos($json_key, '_image')) { ?>
                                                        <span class="name"><?=$json_key?> :: <?=$json_value?> </span>
                                                    <?php } ?>
                                                <?php } */ ?>
                                                <span class="name"><?php echo humanize($value->code); ?></span>
                                                <span class="desc"><?php echo humanize($value->request_type); ?></span>
                                                <?=status_presenter($value->status)?>
                                                
                                            </div>
                                        </a>

                                        <div id="view_modale<?=mb_strtolower($azRange[$key])?>" class="modal-demo">
                                            <button type="button" class="close" onclick="Custombox.close();">
                                                <span>&times;</span><span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="custom-modal-title"><?php echo humanize('Details of Request'); ?></h4>
                                            <div class="custom-modal-text">
                                                <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                    <?php if (!strpos($json_key, '_image')) { ?>
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label"><?=humanize(strtoupper($json_key))?></label>
                                                                <div class="col-md-10"><?=$json_value?> </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="row">
                                                <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                                    <?php if (strpos($json_key, '_image')) { ?>
                                                        <a href="<?=base_url($json_value)?>" target="_blank" title="Proof">
                                                            <img src="<?=base_url($json_value)?>" class="img-thumbnail m_image" alt="profile-image">
                                                        </a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end row -->
