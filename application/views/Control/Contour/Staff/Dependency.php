<style>
    fieldset{
        background-color:#cdd9e6;
        padding:20px 0px 20px 0px ;
    }
</style>
<div class="wrapper">
<div class="container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">  </div>
            <h4 class="page-title"> Dependants </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-8">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> Dependants </h4>
                
                <?php $class = array('id' => 'fileupload'); ?>
                <?php echo form_open_multipart(null , $class); ?>
                    <?=validation_errors()?>
                    <?php $multiple_repeaters = true; ?>
                    <div class="repeater-custom-show-hide">
                        <div data-repeater-list="dependants">
                            <?php foreach($employee_dependants as $dependant){ ?>
                                <div data-repeater-item class="row">
                                    <div class="form-group col-lg-12">
                                        <fieldset>
                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Relationship</label>
                                                <select class="form-control" name="relationship_id" id="relationship" required>
                                                    <?php foreach ( $relationships as $relationship){ ?>
                                                        <option value="<?=$relationship->relationship_id?>" <?=$dependant->relationship_id == $relationship->relationship_id ? "selected" : ""?> ><?=$relationship->description?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Health Benefit?*</label>
                                                <select class="form-control" name="health_benefit" id="health_benefit">
                                                    <option> </option>
                                                    <option value="1" <?=$dependant->health_benefit == true || $dependant->health_benefit == 1? "selected" : ""?> >Yes </option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Full Name*</label>
                                                <input class="form-control" name="full_name" type="text" value="<?=$dependant->full_name?>" required/>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Gender*</label>
                                                <select class="form-control" name="gender" id="gender" required>
                                                    <option value="MALE" <?=$dependant->gender == "MALE" ? "selected" : ""?> >MALE</option>
                                                    <option value="FEMALE" <?=$dependant->gender == "FEMALE" ? "selected" : ""?> > FEMALE</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Date of Birth*</label>
                                                <input class="form-control " name="date_of_birth" type="text" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" value="<?=date('d/m/Y' , strtotime($dependant->date_of_birth))?>" required/>
                                            </div>
                            
                                            <div class="col-sm-1">
                                                <span class="col-lg-12" > - </span>
                                                <span data-repeater-delete class="btn btn-danger btn-lg">
                                                    <span class="glyphicon glyphicon-remove"></span> Delete
                                                </span>
                                            </div>
                                        </fieldset>
                                        <hr >
                                        
                                    </div>
                                </div>
                            <?php } ?>
                            
                            <?php if(count($employee_dependants) == 0){ ?>
                                <fieldset>
                                    <div data-repeater-item class="row">
                                        <div class="form-group col-lg-12">
                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Relationship</label>
                                                <select class="form-control" name="relationship_id" id="relationship_id" required>
                                                    <?php foreach ( $relationships as $relationship){ ?>
                                                        <option value="<?=$relationship->relationship_id?>" ><?=$relationship->description?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Health Benefit?*</label>
                                                <select class="form-control" name="health_benefit" id="relationship">
                                                    <option> </option>
                                                    <option value="1"  >Yes </option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Full Name*</label>
                                                <input class="form-control" name="full_name" type="text" required/>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Gender*</label>
                                                <select class="form-control" name="gender" id="gender" required>
                                                    <option value="MALE" <?=$dependant->gender == "MALE" ? "selected" : ""?> >MALE</option>
                                                    <option value="FEMALE" <?=$dependant->gender == "FEMALE" ? "selected" : ""?> > FEMALE</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="col-lg-12 control-label">Date of Birth*</label>
                                                <input class="form-control " name="date_of_birth" type="text" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required/>
                                            </div>
                            
                                            <div class="col-sm-1">
                                                <span class="col-lg-12" > - </span>
                                                <span data-repeater-delete class="btn btn-danger btn-lg">
                                                    <span class="glyphicon glyphicon-remove"></span> Delete
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <hr >
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-8" >  </div>
                            <div class="col-lg-3">
                                <span data-repeater-create class="btn btn-info btn-md">
                                    <span class="glyphicon glyphicon-plus"></span> Add
                                </span>
                            </div>
                        </div>

                
                        <hr/>
                
                    </div>

                    <?php d_submit($pending); ?>
                <?php echo form_close(); ?>

            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->
