<div class="wrapper">
<div class="container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                 <!--
                <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
                -->
            </div>
            <h4 class="page-title"> Head</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> <!-- --> </h4>
                <?php if($datatable !== null) { ?>
                    <table id="e-table" class="table dataTable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <?php
                                foreach($datatable->header as $heads_string){
                                    echo '<th>'.$heads_string.'</th>';
                                }
                                ?>
                            </tr>
                        </thead>
                    </table>
                <?php } ?>

            </div>
        </div><!-- end col -->


    </div>
    <!-- end row -->