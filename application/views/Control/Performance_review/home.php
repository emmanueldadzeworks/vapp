        <div class="wrapper">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                             <!--
                            <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                            -->
                        </div>
                        <h4 class="page-title"> Performance Review</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Performance Review Form </h4>
                            <style>
                                .fade_out_blend	{ 
                                    width:100%;
                                    height:1000px;
                                    background-color:#FFF
                                }
                            </style>
                            <iframe frameborder="0" class="fade_out_blend" src="<?php echo base_url('control/performance_review/logical_iframe_review'); ?>"><iframe>

                        </div>
                    </div><!-- end col -->


                </div>
                <!-- end row -->