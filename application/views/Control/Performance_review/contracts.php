<div class="wrapper">
<div class="container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                 <!--
                <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
                -->
            </div>
            <h4 class="page-title">Performance Contracts</h4>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-8">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> <!-- --> </h4>
                <?php if($datatable !== null) { ?>
                    <table id="e-table" class="table dataTable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <?php
                                if (count((array)$datatable->header) > 0) {
                                    foreach($datatable->header as $heads_string){
                                        echo '<th>'.$heads_string.'</th>';
                                    }
                                }
                                ?>
                            </tr>
                        </thead>
                    </table>
                <?php } ?>

            </div>
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"> Links </h4>
                <center>
                    <style> .btn_big {border-radius: 0px; padding: 26px 64px;}</style>
                    <?php foreach ($btn_links as $f_key => $f_value){ ?>
                        <button class="btn btn_big waves-effect waves-light" 
                            style="<?=$f_value->disabled ? 'pointer-events: none;cursor: default;opacity: 0.6;' : 'cursor:pointer' ?>" target="_blank"
                            <?php if(!$f_value->disabled) { ?>
                                onclick="window.open('<?=$f_value->url?>','targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500px,height=500px');return" false;=""
                            <?php } ?>
                        >
                            <?=$f_value->name?>
                        </button>
                    <?php } ?>
                </center>
            </div>
        </div><!-- end col -->

        <div class="col-lg-4">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30"> My Leave Requests </h4>

                <ul class="list-group m-b-0 user-list">
                    <?php $azRange = range('A', 'Z'); ?>
                    <?php foreach (array() as $key => $value) { ?>
                        <li class="list-group-item">
                            <a href="#view_modale<?=mb_strtolower($azRange[$key])?>" class="user-list-item waves-effect waves-light m-r-5 m-b-10" style="max-width: 70%;"  data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">
                                <div class="avatar">
                                    <i class="ti-arrow-right"></i>
                                </div>
                                <div class="user-desc">
                                    <?php /*foreach (json_decode($value->json_values) as $json_key => $json_value) { ?>
                                        <?php if (!strpos($json_key, '_image')) { ?>
                                            <span class="name">
                                                <?=humanize($json_key)?> :: 
                                                <?php 
                                                    if ($json_key == "date_request") {
                                                        echo date('Y F', strtotime($this->payslips_m->get((int)$json_value)->start_date)) ;
                                                    } else{
                                                        echo $json_value;
                                                    }
                                                ?>
                                            </span>
                                        <?php } ?>
                                    <?php } */ ?>
                                    <!-- class="desc"<span class="name"  ><?php echo humanize($value->log_type); ?></span>-->
                                    <span class="label label-purple">Requested on <?=$value->created?> </span>
                                    
                                </div>
                            </a>

                            <div id="view_modale<?=mb_strtolower($azRange[$key])?>" class="modal-demo">
                                <button type="button" class="close" onclick="Custombox.close();">
                                    <span>&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="custom-modal-title"><?php echo humanize('Details of Request'); ?></h4>
                                <div class="custom-modal-text">
                                    <?php foreach (json_decode($value->json_encode) as $json_key => $json_value) { ?>
                                        <?php if (!strpos($json_key, '_image')) { ?>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label"><?=humanize(strtoupper($json_key))?></label>
                                                    <div class="col-md-10">
                                                        <?php 
                                                            if ($json_key == "date_request") {
                                                                echo date('Y F', strtotime($this->payslips_m->get((int)$json_value)->start_date)) ;
                                                            } else{
                                                                echo $json_value;
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div><!-- end col -->

    </div>
    <!-- end row -->