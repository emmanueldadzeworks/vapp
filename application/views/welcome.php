<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	$meta = array(
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'description',
				'content' => 'My Great Generic application'
		),
		array(
				'name' => 'keywords',
				'content' => 'view, navigation, form, query'
		),
		array(
				'name' => 'robots',
				'content' => 'no-cache'
		),
		array(
				'name' => 'Content-type',
				'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
		)
	);
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Ghuest House Management System</title>
<?=meta($meta)?>
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>resource/images/logo.png">
<!-- Bootstrap Core CSS -->
<?php echo link_tag('resource/bootstrap/dist/css/bootstrap.min.css'); ?>
<!-- animation CSS -->
<?php echo link_tag('resource/css/animate.css'); ?>
<!-- Custom CSS -->
<?php echo link_tag('resource/css/style.css'); ?>
<!-- color CSS -->
<?php echo link_tag('resource/css/colors/blue.css'); ?>
<style>
	.login-register {
		background: url(<?php echo base_url(); ?>resource/images/login-register.png) center center/cover no-repeat!important;
	}
	.white-box {
		background: #fff0 !important;
	}
	.login-box {
    background: #ffffffc2;
}
</style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
	<?php $loginform = array("id" =>  "loginform" , "class" => "form-horizontal form-material"); ?>
	<?php echo form_open_multipart(null , $loginform); ?>
        <a href="javascript:void(0)" class="text-center db">
        <img src="<?php echo base_url(); ?>resource/images/logo.png" alt="Home" style="height:100px;"/></a>  
        <?=$this->session->flashdata('error')?>
        <?=$this->session->flashdata('success')?>
        <?=validation_errors()?>
        <div class="form-group m-t-40">
          <div class="col-xs-12">
			<input class="form-control"  id="email" name="email" type="email" required="" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
			<input class="form-control" id="password" name="password" type="password" required="" placeholder="Enter Password">
			  
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Remember me </label>
            </div>
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Don't have an account? <a href="<?=base_url('welcome/register')?>" class="text-primary m-l-5"><b>Sign Up</b></a></p>
          </div>
        </div>
	  <?php echo form_close(); ?>


	  <?php $recoverform = array("id" =>  "recoverform" , "class" => "form-horizontal"); ?>
	  <?php echo form_open_multipart(null , $recoverform); ?>
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
	  <?php echo form_close(); ?>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>resource/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>resource/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>resource/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>resource/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>resource/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>resource/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url(); ?>resource/dist/jQuery.style.switcher.js"></script>
</body>

</html>
