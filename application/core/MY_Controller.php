<?php
class MY_Controller extends CI_Controller {
	
	public $data = array();
	public $images = array("web_favicon_icon" , "picture" , "company_logo" , "logo" , "image"  , "icon" , "photo"  , "background_photo" , "image_icon" , "cover_picture");
	public $download = array("download_file" );
	public $documents = array("certification_document" , "pro_formal_invoice");
	public $video_download = array("video");
	public $public_default_folder = array("root_folder" => "Gallery");
	public $public_path = array("root_path" => "Gallery/Public");
	public $path = array("root_path" => "Gallery/Menus");
	public $video_path = "Video";
	public $Download_path = "Download";
	public $product_path = array("root_path" => "Gallery/Products");
	public $portfolio_path = array("root_path" => "Gallery/Portfolio");
	
	function __construct() {
		parent::__construct();
		$this->load->model('resource_m');
		$this->load->library('migration');
		$this->Migration();
        //$this->load->library("PHPMailer_Library");
        //$this->objMail = $this->phpmailer_library->load();


		$this->api_key = config_item("api_key");

	}
	
	public function Migration()
	{
		if ($this->migration->current() === FALSE)
		{
			show_error($this->migration->error_string());
		}
	}


	public function upload ($filler)
	{
			$type = explode('.', $_FILES[$filler]["name"]);
			$type = strtolower($type[count($type)-1]);
			$url = "resource/image/".uniqid(rand()).'.'.$type;

			if(in_array($type, array("jpg", "jpeg", "png", "pdf")))
			{
				if(is_uploaded_file($_FILES[$filler]["tmp_name"]))
				{
					if(move_uploaded_file($_FILES[$filler]["tmp_name"],$url))
					{
						return $url;
					}
				}
			}
	}



	public function file_contents ($src = null )
	{ 
		$return = null;
		if($src !== null) {
			foreach ($src as $key => $value){
				$type = explode('.', $value );
				$type = strtolower($type[count($type)-1]);
				$document = bin2hex(file_get_contents($value));
				$data['resource'] = $document;
				$data['extension'] = $type;
				$return["binary_document_".$key] = $this->resource_m->save($data);
			}
		}        
		return $return;

	}


	public function m_upload ($filter ,$filterf , $fillers, $fillert)
	{
			$type = explode('.', $_FILES[$filter]["name"][$fillers][$fillert]);
			$type = strtolower($type[count($type)-1]);
			$url = "resource/image/".uniqid(rand()).'.'.$type;
			if(in_array($type, array("jpg", "jpeg", "png", "pdf")))
			{
				if(is_uploaded_file($_FILES[$filter]["tmp_name"][$fillers][$fillert]))
				{
					if(move_uploaded_file($_FILES[$filter]["tmp_name"][$fillers][$fillert],$url))
					{
						return $url;
					}
				}
			}
			else {
				$this->data['notify'][] = array('title' => 'Sorry <hr> File Format not accepted </hr>', 'message' => _l('Please try again'), 'type' => 'error' );

			}
	}

	public function multiple_files ($data)
	{
		$counter=0;
		$return_data = null;
		foreach ($data as $key => $value) {
			foreach ($value as $fkey => $fvalue) {
				foreach ($fvalue as $skey => $svalue) {
					foreach ($svalue as $tkey => $tvalue) {
						if (!empty($data[$key]["name"][$skey][$tkey])) {
							$return_data['File_image_path_0'.$counter] = $this->m_upload($key,$fkey,$skey,$tkey);
							$counter++;
						}
					}
				}
			}
		}
		if ($return_data !== null) {
			foreach ($return_data as $key => $value) {
				if ($value==null) {
					unset($return_data[$key]);
				}
			}
		}
		return $return_data;
	}

	public function database_location_file ($data, $sub_string = false)
	{
		if ($sub_string) {
			foreach ($data as $key => $value) {
				if (!empty($value["name"])) {
					$data[$key] = $this->upload($key);
				} else {
					unset($data[$key]);
				}
			}
		}

		
		foreach ($this->images as $key => $value) {
			if(isset($_POST[$value.'_empty'])){
				unlink($_POST[$value.'_empty']);
				unset($data[$value.'_empty']);
				$data[$value] = '';
			}
			else
			{
				if (!empty($_FILES[$value]["name"])) {
				$data[$value] = $this->upload($value);
				}
			}
		}

		foreach ($this->download as $key => $value) {
			if(isset($_POST[$value.'_empty'])){
				unlink($_POST[$value.'_empty']);
				unset($data[$value.'_empty']);
				$data[$value] = '';
			}
			else
			{
				if (!empty($_FILES[$value]["name"])) {
				$data[$value] = $this->alt_upload_download($value);
				}
			}
		}

		foreach ($this->documents as $key => $value) {
			if(isset($_POST[$value.'_empty'])){
				unlink($_POST[$value.'_empty']);
				unset($data[$value.'_empty']);
				$data[$value] = '';
			}
			else
			{
				if (!empty($_FILES[$value]["name"])) {
					$data[$value] = $this->upload_doc($value);
				}
			}
		}

		foreach ($this->video_download as $key => $value) {
			if(isset($_POST[$value.'_empty'])){
				unlink($_POST[$value.'_empty']);
				unset($data[$value.'_empty']);
				$data[$value] = '';
			}
			else
			{
				if (!empty($_FILES[$value]["name"])) {
					$data[$value] = $this->upload_video($value);
					if ($data[$value] == false) {
						$this->session->set_flashdata("error", "Oops , Sorry <p>Upload failed!</p> post_max_size = ".ini_get('post_max_size')."<p>".$this->upload->display_errors());
					}
				}
			}
		}
		if (isset($data['password'])) {
			if ($data['password'] == "") {
				unset($data['password']);
				unset($data['password_confirm']);
				# code...
			} 
		}
		unset($data['password_confirm']);
		
		return $data;
	}
}


